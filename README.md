# meta

meta is a tool dedicated to handle a coder's workspace.

If you are like me, you will checkout your project workspace from your favorite dvcs server, without any actual source files, just configurations, makefiles etc.  You will also checkout all your dependencies, wether you own them, or by vendor branches.

    git clone dvcs.org:/myproject.git
    git clone dvcs.org:/module1.git src/myteam/module1
    git clone dvcs.org:/module2.git src/thirdparty/module2

so you, and you team mates, can collaborate.

Very soon, you will need to:

  - checkout all the repositories before getting started (checked)
  - fetch the work from your teammate, on every dependency
  - tag all dependencies, when you release a version
  - prune obsolete dependencies
  - ...

That's exactly the goal of `meta`.

It just needs a dependencies file, aka `.metafile`, where all  *dependencies* are described.

`meta` is not very opinionated, and basically run two kinds of operations:

   - Checkout/Prune dependencies
   - Run any arbitrary command of your own

for instance

    meta git pull

will be executed on every dependency directory, hence, your code base will be uptodate. If you use Mercurial, you would rather do

    meta hg pull


## Checkout/Prune

whenever a command is executed, `meta` look for the actual state of dependencies.

  - If a dependency is missing, it will try to create it
  - If a dependency was removed, but still in the way it can prune it.

both process are controlled in different way.

### dependency creation

Whenever a dependency is missing on the disk, a command ( `mkdir` defined in the `.metafile` ) will be executed, to create the dependency.

The command can be any arbitrary command, except that it is executed in the root of the project (not in the dependency path).

See Command executions for more detail

### dependency pruning

Whenever a dependency is found in the disk, but was marked as deleted (parked) in the `.metafile`, it can be pruned. 
If the `meta` command was started using `--prune` argument it will actually remove it (unless, `--dry` has been specified)
Otherwise, a message is printed out.


## Commands

commands are any arbitrary golang   [text/template](http://golang.org/pkg/text/template/) where the template's host structure is the json object declared in the `.metafile`.`path` map.

for instance: 
with this  `.metafile`
    
    {
        "mkdir": "git clone {{.remote}} -b {{.branch}} {{.path}}",
        "path": {
            "src/powerteam/uid": {
                "remote": "git@bitbucket.org:powerteam/uid.git",
                "branch": "dev"
            }
        }
    }

you'll get:

    $ meta echo {{.branch}}
    src/powerteam/uid: echo dev
    dev

    

## .metafile format

it's a json object file format.

key/value:

  - `"mkdir"` > string : the mkdir command
  - `"templates"` > object : 
      + template name > string: the template
  - `"path"` > object :
      + path to the sub dir > object :
          * any json object
  - `"park"` > array of string

Here is a minimalistic `.metafile`
    
    {
        "mkdir": "git clone {{.remote}} -b {{.branch}} {{.path}}",
        "path": {
            "src/powerteam/uid": {
                "remote": "git@bitbucket.org:powerteam/uid.git",
                "branch": "dev"
            }
        }
    }


