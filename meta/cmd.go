package meta

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"
)

//Cmd represent a single path command
type Cmd interface {
	// execute the parked command
	Run(command string)
	// create the Cmd using the "mkdir" command. Nota Bene, the 'mkdir' is in fact any command that would "create" the directory, it is more likely to be "git clone"
	Checkout()
	// return a textual representation of the current .metafile status
	Status() string
	fmt.Stringer
}

// a cmd is "almost" the equivalent of exec.Cmd except that its execution cannot fail, instead, it has an error message
type cmd struct {
	path      string
	values    map[string]interface{}
	mkdir     string
	location  string // root path
	buffered  bool
	templates map[string]string // all "bank" templates

	err    error
	buffer bytes.Buffer
}

//New Cmd: a Cmd is a single meta command:
// it has:
//   - path the relative subproject path
//   - values: the map defined in the .metafile
//   - tmpl the template to be executed
//   - location the root directory for the meta project.
//   - if 'quiet' no input or output are made, everything is buffered.
func NewCmd(path string, values map[string]interface{}, location string, quiet bool, mkdir string, templates map[string]string) Cmd {

	cmd := &cmd{
		path:      path,
		values:    values,
		mkdir:     mkdir,
		location:  location,
		buffered:  quiet,
		templates: templates,
	}
	cmd.values["path"] = filepath.Join(location, path)
	return cmd

}

func (m *cmd) String() string {
	return string(m.buffer.Bytes())
}

func (m *cmd) Exists() (path string, exists bool) {
	path = filepath.Join(m.location, m.path)
	_, e := os.Stat(path)
	exists = !os.IsNotExist(e)
	return
}

func (m *cmd) Status() (status string) {

	s := "missing" // by default
	if _, exists := m.Exists(); exists {
		s = "exists"
	}

	js, err := json.MarshalIndent(m.values, "    ", "    ")
	if err != nil {
		return fmt.Sprintf("%s : %s\n%s", m.path, s, err.Error())
	} else {
		return fmt.Sprintf("%s : %s\n%s", m.path, s, js)
	}
}

func (m *cmd) Checkout() {
	dir := filepath.Join(m.location, m.path)
	if _, e := os.Stat(dir); os.IsNotExist(e) {
		m.run(m.location, m.mkdir)
	}
}

func (m *cmd) Run(command string) {

	dir := filepath.Join(m.location, m.path)
	if _, e := os.Stat(dir); os.IsNotExist(e) {
		m.err = fmt.Errorf("path does not exist %s", dir)
		return
	}
	m.run(dir, command)
}

// run any template
func (m *cmd) run(dir string, command string) {

	//first check that the directory exists (it should have been created before by the make command)

	tmpl, err := m.compile(command)
	if err != nil {
		m.err = fmt.Errorf("template syntax error: %s", err.Error())
		return
	}

	// run the template
	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, m.values)
	if err != nil {
		m.err = fmt.Errorf("template error: %s", err.Error())
		return
	}
	// yeappy I've got the cmd all alone
	cmd := buf.String()
	// now make the command
	x := exec.Command("sh", "-c", cmd)
	x.Dir = dir
	// streams are defaulted to stdin and out

	if m.buffered {
		x.Stdout = &m.buffer
		//x.Stdin = os.DevNull
	} else {
		x.Stdout = io.MultiWriter(os.Stdout, &m.buffer)
		x.Stdin = os.Stdin
	}

	x.Stderr = x.Stdout
	fmt.Fprintf(x.Stdout, "%s$ %s\n", dir, cmd)
	if !*dry {
		err = x.Run() // later provide a true async way (start instead of run, and buffers instead of stdout)
		if err != nil {
			m.err = fmt.Errorf("Command Error %s", err.Error())
			return
		}
	} else {
		fmt.Fprintf(x.Stdout, "dry-run")
	}
}

func (m *cmd) compile(root string) (tmpl *template.Template, err error) {

	for name, txt := range m.templates {

		if tmpl == nil {
			tmpl, err = template.New(name).Parse(txt)
		} else {
			tmpl, err = tmpl.New(name).Parse(txt)
		}
		if err != nil {
			return
		}
	}
	tmpl, err = tmpl.New("root").Parse(root)
	if err != nil {
		return
	}
	return
}
