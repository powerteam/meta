package meta

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

const (
	Filename = ".metafile"
)

var (
	async = flag.Bool("async", false, "start all commands concurrently")
	prune = flag.Bool("prune", false, "prune existing directories in the park list")
	alias = flag.Bool("alias", false, "args are considered as alias to one of the templates defined in the metafile")
	dry   = flag.Bool("dry", false, "do nothing, just print out")
)

//Execute parses the command line and run the meta tool
func Execute() {
	flag.Parse()
	meta, err := Find()
	if err != nil {
		err = fmt.Errorf("Cannot find .metafile from current directory.", err.Error())
		fmt.Println(err.Error())
		os.Exit(-1)
	}
	//prune parked dir before all
	if *prune {
		meta.Prune()
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(-1)
		}
	}

	//make sure that all dir exists
	meta.Checkout() // always try to checkout stuff

	if flag.NArg() > 0 { // run only if there is a command

		//default
		var cmd string
		if *alias { // subst every arg as an alias
			calls := make([]string, flag.NArg())
			for i, arg := range flag.Args() {
				if val, ok := meta.Templates[arg]; ok {
					calls[i] = val //brute substitution
				} else {
					calls[i] = arg
				}
			}
			cmd = strings.Join(calls, "")
		} else {
			cmd = strings.Join(flag.Args(), " ")
		}
		meta.Run(cmd)
	}
}

//LookupFile for .metafile starting from '.' upwards.
func lookupFile() (metafile string, err error) {
	var current string
	current, err = os.Getwd()
	for err == nil {
		metafile = filepath.Join(current, Filename) //expected file
		_, e := os.Stat(metafile)
		if e == nil { // wouhou it exists
			return
		} else if os.IsNotExist(e) { // catch the non existence
			// try the parent
			newcurrent := filepath.Dir(current)
			if newcurrent == current {
				err = errors.New("File Not Found")
			}
			current = newcurrent
		} else { // any other case are "wrong"
			err = e // will stop the loop
		}
	}
	return
}

// Find in the current Working Dir, or it's parents the .metafile and allocate
// the corresponding instance.
func Find() (meta *Meta, err error) {
	metafile, err := lookupFile()
	if err != nil {
		err = fmt.Errorf("Not found: %s\n", err.Error())
		return
	}

	meta, err = NewMeta(metafile)
	if err != nil {
		err = fmt.Errorf("Invalid File Format: %s\n", err.Error())
		return
	}
	return

}

// Meta provide tools to deal with sub path, once instanciated it can run several top level commands
type Meta struct {
	Path      map[string]map[string]interface{} `json:"path,omitempty"`
	Park      []string                          `json:"park,omitempty"`
	Templates map[string]string                 `json:"templates,omitempty"`
	Mkdir     string                            `json:"mkdir,omitempty"`
	location  string                            //actual dir of the current meta file
	cmds      map[string]Cmd                    // operational command
}

//NewMeta allocate a new instance of Meta
func NewMeta(metafile string) (meta *Meta, err error) {
	meta = new(Meta)
	data, err := ioutil.ReadFile(metafile)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, meta)
	meta.location = filepath.Dir(metafile)

	meta.cmds = make(map[string]Cmd, len(meta.Path))
	for path, values := range meta.Path {
		meta.cmds[path] = NewCmd(path, values, meta.location, *async, meta.Mkdir, meta.Templates)
	}
	return
}

// make the directory structure if needed
func (m *Meta) Prune() (err error) {
	for _, k := range m.Park {
		//first check that the directory exists, create it if needed
		dir := filepath.Join(m.location, k)
		if _, e := os.Stat(dir); !os.IsNotExist(e) {
			// special case, invoke the "maker" for the dir

			// I'm gonna run Mkdir template instead
			log.Printf("rm -Rf %s", dir)
			if !*dry {
				os.RemoveAll(dir)
			}
		}
	}
	return
}

func (m *Meta) Run(command string) {
	if *async {
		p := make(chan string)
		go func() {
			for {
				msg := <-p
				if msg != "" {
					fmt.Println(msg)
				}
			}
		}()

		var wg sync.WaitGroup // a wait group to wait until everything is checkouted
		for _, cmd := range m.cmds {
			wg.Add(1)
			go func(cmd Cmd) {
				defer wg.Done()
				cmd.Run(command)
				p <- cmd.String()
			}(cmd)
		}
		wg.Wait() // wait for all goroutine to have terminated

	} else {

		for _, cmd := range m.cmds {
			cmd.Run(command)
		}
	}

}

//Checkout = create missing directory, by running the "mkdir" command
func (m *Meta) Checkout() {
	if *async {
		p := make(chan string)
		go func() {
			for {
				msg := <-p
				if msg != "" {
					fmt.Println(msg)
				}
			}
		}()

		var wg sync.WaitGroup // a wait group to wait until everything is checkouted
		for _, cmd := range m.cmds {
			wg.Add(1)
			go func(cmd Cmd) {
				defer wg.Done()
				cmd.Checkout()
				p <- cmd.String()
			}(cmd)
		}

		wg.Wait() // wait for all gorutine to have terminated

	} else {

		for _, cmd := range m.cmds {
			cmd.Checkout()
		}
	}

}

//List just print out the current system status
func (m *Meta) List() {
	for _, cmd := range m.cmds {
		fmt.Println(cmd.Status())
		//TODO print result
	}

}
